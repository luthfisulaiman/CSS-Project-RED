package com.example.luthfi.red;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class ResultActivity extends AppCompatActivity {

  Handler handler;
  Runnable runnable;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result);
    showLoadingScreen();
  }

  private void showLoadingScreen() {
    openFragment(new LoadingFragment());
    handler = new Handler();
    runnable = new Runnable() {
      @Override
      public void run() {
        //Second fragment after 2 seconds appears
        openFragment(new ResultFragment());
      }
    };

    handler.postDelayed(runnable, 2000);
  }

  private void openFragment(final Fragment fragment)   {
    FragmentManager fragmentManager = getFragmentManager();
    FragmentTransaction transaction = fragmentManager.beginTransaction();
    transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
    transaction.replace(R.id.rotating_image_loading, fragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }

  @Override
  public void onDestroy() {
    handler.removeCallbacks(runnable);
    super.onDestroy();
  }
}
