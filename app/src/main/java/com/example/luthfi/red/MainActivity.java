package com.example.luthfi.red;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import com.example.luthfi.red.R;


public class MainActivity extends AppCompatActivity {

  private ImageButton btnCamera;
  private ImageButton btnCamera2;

  private boolean isCameraOpened = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    System.out.println("test");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnCamera = (ImageButton) findViewById(R.id.btnCamera);
    btnCamera2 = (ImageButton) findViewById(R.id.btnCamera2);

    btnCamera.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        isCameraOpened = true;
        openCamera();
      }
    });

    btnCamera2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        isCameraOpened = true;
        openCamera();
      }
    });
  }

  private void openCamera() {
    Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
    startActivityForResult(intent, 0);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    // TODO Auto-generated method stub
    super.onActivityResult(requestCode, resultCode, data);
    if(isCameraOpened) {
      isCameraOpened = false;
      startActivity(new Intent(this, ResultActivity.class));
    }
  }
}