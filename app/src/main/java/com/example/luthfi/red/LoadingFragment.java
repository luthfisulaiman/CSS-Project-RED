package com.example.luthfi.red;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadingFragment extends Fragment {

  @BindView(R.id.rotate_image)
  ImageView rotateImage;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_loading_screen, container, false);

    ButterKnife.bind(this, view);
    animate();

    return view;
  }

  private void animate() {
    Animation startRotateAnimation = AnimationUtils
        .loadAnimation(getActivity().getApplicationContext(), R.anim.android_rotate_animation);
    startRotateAnimation
        .setInterpolator(getActivity().getApplicationContext(), android.R.anim.linear_interpolator);
    rotateImage.startAnimation(startRotateAnimation);
  }
}