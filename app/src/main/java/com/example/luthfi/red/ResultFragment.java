package com.example.luthfi.red;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.Random;

public class ResultFragment extends Fragment {

  final int min = 10;
  final int max = 10000;
  Random r = new Random();

  @BindView(R.id.video_value)
  TextView videoValue;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_result, container, false);

    ButterKnife.bind(this, view);
    setValue();
    return view;
  }

  private void setValue() {
    final int random = r.nextInt((max - min) + 1) + min;

    videoValue.setText("Rp " + random + "000");
  }

  @OnClick(R.id.back_to_home)
  public void onClickHomeButton(View v) {
    Intent intent = new Intent(getActivity(), MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent);
  }
}